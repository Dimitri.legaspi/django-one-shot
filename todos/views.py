from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos}
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todo}
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoForm(instance=post)

    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/update.html", context)


def delete_todo_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/new_item.html", context)


def update_todo_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoItemForm(instance=post)

    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
